WEBDIR=/var/www/htdocs/

publish:
	@if [ "$$(hostname)" = "azarus.ch" ]; then\
		rsync -av --exclude-from exclude-file ./ $(WEBDIR);\
	else\
		rsync -av --exclude-from exclude-file ./ vps:/$(WEBDIR);\
	fi

newpost:
	@printf "Enter post title filename: "
	@read TITLE; cp post.tmpl "posts/$$(date +%F)-$$TITLE.html"
